Quotes project with simple java core and servlet technologies.
Using :
- java SE 8
- JDBC for DB connection
- JSP and JSTL for visual part
- filter based security
- ajax for change rating requests
- i18n with jstl <fmt>, localizationContext