package ua.karghoff.quotes.util;

import ua.karghoff.quotes.entity.Role;
import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.service.impl.UserService;
import ua.karghoff.quotes.db.exception.DuplicateEntryException;
import ua.karghoff.quotes.service.exception.FieldsValidateException;

public class Registration {
    UserService service = new UserService();

    public User register(String name, String password)
            throws FieldsValidateException {

        if ((name == null || password == null) ||
                (name.isEmpty()) || password.isEmpty()) {
            throw new FieldsValidateException();
        }

        User u = service.getByName(name);
        if (u != null) {
            throw new DuplicateEntryException();
        }

        User user = new User();
        user.setPassword(Hasher.hash(password));
        user.setName(name);
        user.setRole(Role.USER);

        service.insert(user);

        return service.getByName(name);
    }
}
