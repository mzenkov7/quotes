package ua.karghoff.quotes.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {
    private static final String algorithm = "SHA-256";
    private static final String ENCODE = "UTF-8";

    private Hasher() {
        throw new RuntimeException();
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            for (String s : args) {
                System.out.println("Origin : " + s);
                System.out.println("Salted : " + Hasher.hash(s));
                System.out.println();
            }
        }
    }

    public static String hash(String base) {
        String salt = "sch";
        String s = base;
        for (int i = 0; i < 10; i++) {
            s = sha256(s + salt);
        }
        return s;
    }

    private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] hash = digest.digest(base.getBytes(ENCODE));
            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
}
