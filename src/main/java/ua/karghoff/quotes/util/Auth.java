package ua.karghoff.quotes.util;

import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.service.impl.UserService;
import ua.karghoff.quotes.service.exception.AuthenticationException;
import ua.karghoff.quotes.service.exception.FieldsValidateException;

public class Auth {
    private UserService service = new UserService();

    public User login(String login, String password)
            throws FieldsValidateException, AuthenticationException {
        if ((login == null || password == null)
                || (login.isEmpty() || password.isEmpty())) {
            throw new FieldsValidateException();
        }

        User user = service.getByName(login);
        password = Hasher.hash(password);

        if (user == null || !user.getPassword().equals(password)) {
            throw new AuthenticationException();
        }
        return user;
    }
}
