package ua.karghoff.quotes.db.exception;

public class DatabaseException extends RuntimeException {

    public DatabaseException() {
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }
}
