package ua.karghoff.quotes.db.dao;

import ua.karghoff.quotes.entity.Entity;

import java.util.List;

public interface SortedDAO<T extends Entity> extends DAO<T> {

    List<T> getRandom(int limit);

    List<T> getSorted(String columnName, boolean reverse, int offset, int limit);
}
