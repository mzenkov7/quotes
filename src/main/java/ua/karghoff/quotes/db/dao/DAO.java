package ua.karghoff.quotes.db.dao;

import ua.karghoff.quotes.entity.Entity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface DAO<T extends Entity> {

    T getById(Long id);

    int insert(T entity);

    int delete(T entity);

    int update(T entity);

    List<T> getAll(int offset, int limit);

    long count();

    Connection getConnection() throws SQLException;
}
