package ua.karghoff.quotes.db.dao;

import ua.karghoff.quotes.entity.NamedEntity;

public interface NamedDAO<T extends NamedEntity> extends DAO<T> {

    T getByName(String name);
}
