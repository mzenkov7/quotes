package ua.karghoff.quotes.db.dao.impl;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import ua.karghoff.quotes.db.exception.CreateEntityException;
import ua.karghoff.quotes.db.exception.DatabaseException;
import ua.karghoff.quotes.entity.Role;
import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.db.exception.DuplicateEntryException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO extends GenericNamedDAO<User> {

    @Override
    public int insert(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO " + getTableName() + " (name, password, role) VALUES (?,?,?)");

            int param = 0;
            statement.setString(++param, user.getName());
            statement.setString(++param, user.getPassword());
            statement.setString(++param, user.getRole().toString());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    @Override
    public int update(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE " + getTableName() + " SET name = ?, password = ?, " +
                            "role = ? WHERE id = ?");
            int param = 0;
            statement.setString(++param, user.getName());
            statement.setString(++param, user.getPassword());
            statement.setString(++param, user.getRole().toString());
            statement.setLong(++param, user.getId());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    @Override
    public User extract(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getLong("id"));
            user.setPassword(rs.getString("password"));
            user.setName(rs.getString("name"));
            user.setRole(Role.valueOf(rs.getString("role")));
        } catch (SQLException e) {
            throw new CreateEntityException();
        }
        return user;
    }

    @Override
    protected String getTableName() {
        return "user";
    }
}
