package ua.karghoff.quotes.db.dao.impl;

import ua.karghoff.quotes.db.dao.DAO;
import ua.karghoff.quotes.db.exception.ConnectionException;
import ua.karghoff.quotes.db.exception.DatabaseException;
import ua.karghoff.quotes.entity.Entity;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericDAO<T extends Entity> implements DAO<T> {
    private static final String SELECT_COUNT = "SELECT count(*) FROM %s";
    private static final String SELECT_ALL = "SELECT * FROM %s LIMIT ?,?";
    private static final String SELECT_BY_ID = "SELECT * FROM %s WHERE id = ?";
    private static final String DELETE = "DELETE FROM %s WHERE id = ?";

    @Override
    public abstract int insert(T entity);

    @Override
    public abstract int update(T entity);

    @Override
    public T getById(Long id) {
        T entity = null;
        try (Connection connection = getConnection()) {
            final String selectQuery = String.format(SELECT_BY_ID, getTableName());
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                entity = extract(rs);
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return entity;
    }

    @Override
    public int delete(T entity) {
        try (Connection connection = getConnection()) {
            final String deleteQuery = String.format(DELETE, getTableName());
            try (PreparedStatement statement = connection.prepareStatement(deleteQuery)) {
                statement.setLong(1, entity.getId());
                return statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    @Override
    public List<T> getAll(int offset, int limit) {
        List<T> list = new ArrayList<>();
        try (Connection connection = getConnection()) {
            final String selectQuery = String.format(SELECT_ALL, getTableName());
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            int param = 0;
            statement.setInt(++param, offset);
            statement.setInt(++param, limit);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(extract(rs));
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return list;
    }

    @Override
    public Connection getConnection() throws SQLException {
        try {
            Context initContext = new InitialContext();
            // using Tomcat connection pool
            // from META-INF/context.xml
            Context webContext = (Context) initContext.lookup("java:/comp/env");
            DataSource datasource = (DataSource) webContext.lookup("jdbc/dataSource");
            Connection connection = datasource.getConnection();
            if (connection == null) {
                throw new ConnectionException();
            }
            return connection;
        } catch (NamingException e) {
            System.err.println("Cannot get JNDI DataSource");
            return null;
        }
    }

    @Override
    public long count() {
        long count = 0;
        try (Connection connection = getConnection()) {
            final String selectQuery = String.format(SELECT_COUNT, getTableName());
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                count = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return count;
    }

    abstract protected T extract(ResultSet rs);

    abstract protected String getTableName();
}
