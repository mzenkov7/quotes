package ua.karghoff.quotes.db.dao.impl;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import ua.karghoff.quotes.db.exception.DatabaseException;
import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.db.dao.DAO;
import ua.karghoff.quotes.db.exception.CreateEntityException;
import ua.karghoff.quotes.db.exception.DuplicateEntryException;
import ua.karghoff.quotes.entity.Quote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QuoteDAO extends GenericSortedDAO<Quote> {

    @Override
    public int insert(Quote quote) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO " + getTableName() + " (text, author, rating, user_id, date) VALUES (?,?,?,?,?)");

            int param = 0;
            statement.setString(++param, quote.getText());
            statement.setString(++param, quote.getAuthor());
            statement.setInt(++param, quote.getRating());
            statement.setLong(++param, quote.getUser().getId());
            statement.setTimestamp(++param, quote.getDate());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    @Override
     public int update(Quote quote) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE " + getTableName() + " SET text = ?, author = ?, " +
                            "rating = ?, user_id = ?, date = ? WHERE id = ?");
            int param = 0;
            statement.setString(++param, quote.getText());
            statement.setString(++param, quote.getAuthor());
            statement.setInt(++param, quote.getRating());
            statement.setLong(++param, quote.getUser().getId());
            statement.setTimestamp(++param, quote.getDate());
            statement.setLong(++param, quote.getId());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    public int changeRating(Quote quote, int diff) {
        if (diff == 0) {
            return 0;
        }
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE " + getTableName() + " SET rating = rating + ? WHERE id = ?");
            int param = 0;
            statement.setInt(++param, diff);
            statement.setLong(++param, quote.getId());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    @Override
    public Quote extract(ResultSet rs) {
        DAO<User> userDAO = new UserDAO();

        Quote quote = new Quote();
        try {
            quote.setId(rs.getLong("id"));
            quote.setText(rs.getString("text"));
            quote.setAuthor(rs.getString("author"));
            quote.setRating(rs.getInt("rating"));

            long userId = rs.getLong("user_id");
            quote.setUser(userDAO.getById(userId));

            quote.setDate(rs.getTimestamp("date"));
        } catch (SQLException e) {
            throw new CreateEntityException();
        }
        return quote;
    }

    @Override
    protected String getTableName() {
        return "quote";
    }
}
