package ua.karghoff.quotes.db.dao.impl;

import ua.karghoff.quotes.db.dao.SortedDAO;
import ua.karghoff.quotes.db.exception.DatabaseException;
import ua.karghoff.quotes.entity.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericSortedDAO<T extends Entity> extends GenericDAO<T> implements SortedDAO<T> {
    private static final String SELECT_SORTED = "SELECT * FROM %s ORDER BY %s %s LIMIT ?,?";
    private static final String SELECT_RANDOM = "SELECT * FROM %s ORDER BY RAND() LIMIT ?";

    @Override
    public List<T> getSorted(String columnName, boolean reverse, int offset, int limit) {
        List<T> users = new ArrayList<>();
        try (Connection connection = getConnection()) {
            final String order = reverse ? "DESC" : "ASC";
            final String selectQuery = String.format(SELECT_SORTED, getTableName(), columnName, order);
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            int param = 0;
            statement.setInt(++param, offset);
            statement.setInt(++param, limit);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                users.add(extract(rs));
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        return users;
    }

    @Override
    public List<T> getRandom(int limit) {
        List<T> users = new ArrayList<>();
        try (Connection connection = getConnection()) {
            final String selectQuery = String.format(SELECT_RANDOM, getTableName());
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            int param = 0;
            statement.setInt(++param, limit);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                users.add(extract(rs));
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        return users;
    }
}
