package ua.karghoff.quotes.db.dao.impl;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import ua.karghoff.quotes.db.exception.CreateEntityException;
import ua.karghoff.quotes.db.exception.DatabaseException;
import ua.karghoff.quotes.entity.Comment;
import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.db.dao.DAO;
import ua.karghoff.quotes.db.exception.DuplicateEntryException;
import ua.karghoff.quotes.entity.Quote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO extends GenericSortedDAO<Comment> {
    private static final String SELECT_BY_QUOTE_ID = "SELECT * FROM %s WHERE quote_id = ? ORDER BY date ASC";

    @Override
    public int insert(Comment comment) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO " + getTableName() + " (quote_id, user_id, text, date) VALUES (?,?,?,?)");

            int param = 0;
            statement.setLong(++param, comment.getQuote().getId());
            statement.setLong(++param, comment.getUser().getId());
            statement.setString(++param, comment.getText());
            statement.setTimestamp(++param, comment.getDate());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    @Override
    public int update(Comment comment) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE " + getTableName() + " SET quote_id = ?, user_id = ?, " +
                            "text = ?, date = ? WHERE id = ?");
            int param = 0;
            statement.setLong(++param, comment.getQuote().getId());
            statement.setLong(++param, comment.getUser().getId());
            statement.setString(++param, comment.getText());
            statement.setTimestamp(++param, comment.getDate());
            statement.setLong(++param, comment.getId());
            return statement.executeUpdate();
        } catch (MySQLIntegrityConstraintViolationException e) {
            throw new DuplicateEntryException();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
    }

    public List<Comment> getByQuote(Quote quote) {
        List<Comment> list = new ArrayList<>();
        try (Connection connection = getConnection()) {
            final String selectQuery = String.format(SELECT_BY_QUOTE_ID, getTableName());
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            statement.setLong(1, quote.getId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(extract(rs));
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return list;
    }

    @Override
    protected Comment extract(ResultSet rs) {
        DAO<User> userDAO = new UserDAO();
        DAO<Quote> quoteDAO = new QuoteDAO();

        Comment comment = new Comment();
        try {
            comment.setId(rs.getLong("id"));

            long userId = rs.getLong("user_id");
            comment.setUser(userDAO.getById(userId));
            long quoteId = rs.getLong("quote_id");
            comment.setQuote(quoteDAO.getById(quoteId));

            comment.setText(rs.getString("text"));
            comment.setDate(rs.getTimestamp("date"));
        } catch (SQLException e) {
            throw new CreateEntityException();
        }
        return comment;
    }

    @Override
    protected String getTableName() {
        return "comment";
    }
}
