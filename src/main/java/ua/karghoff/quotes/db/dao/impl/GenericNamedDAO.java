package ua.karghoff.quotes.db.dao.impl;

import ua.karghoff.quotes.db.dao.NamedDAO;
import ua.karghoff.quotes.db.exception.DatabaseException;
import ua.karghoff.quotes.entity.NamedEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class GenericNamedDAO<T extends NamedEntity> extends GenericDAO<T> implements NamedDAO<T> {
    private static final String SELECT_BY_NAME = "SELECT * FROM %s WHERE name = ? LIMIT 1";

    @Override
    public T getByName(String name) {
        T entity = null;
        try (Connection connection = getConnection()) {
            final String selectQuery = String.format(SELECT_BY_NAME, getTableName());
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                entity = extract(rs);
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return entity;
    }
}
