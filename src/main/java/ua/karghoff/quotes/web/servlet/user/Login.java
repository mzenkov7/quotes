package ua.karghoff.quotes.web.servlet.user;

import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.service.exception.AuthenticationException;
import ua.karghoff.quotes.service.exception.FieldsValidateException;
import ua.karghoff.quotes.util.Auth;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(displayName = "Login servlet", urlPatterns = "/login")
public class Login extends HttpServlet {
    private static final String ROOT = "/";
    private static final String LOGIN_PAGE = "/login.jsp";
    private static final String USER_SESSION_ATTRIBUTE = "user";

    public String getHome(User user) {
        if (user == null) {
            return LOGIN_PAGE;
        } else {
            return ROOT;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Auth auth = new Auth();

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute(USER_SESSION_ATTRIBUTE);
        try {
            if (user != null) {
                session.removeAttribute(USER_SESSION_ATTRIBUTE);
            }
            user = auth.login(login, password);
            session.setAttribute(USER_SESSION_ATTRIBUTE, user);
        } catch (FieldsValidateException e) {
            session.setAttribute("fieldError", "error");
        } catch (AuthenticationException e) {
            session.setAttribute("loginError", "error");
        } finally {
            response.sendRedirect(request.getContextPath() + getHome(user));
        }
    }
}
