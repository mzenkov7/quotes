package ua.karghoff.quotes.web.servlet.comment;

import ua.karghoff.quotes.entity.Comment;
import ua.karghoff.quotes.service.impl.CommentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "EditComment", urlPatterns = "/comment/edit/*")
public class EditComment extends HttpServlet {
    private static final String NOT_FOUND = "/error/404.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommentService service = new CommentService();

        long id = Long.parseLong(request.getParameter("comment-id"));
        String text = request.getParameter("text");

        String target;

        Comment comment = service.getById(id);
        if (text == null || text.trim().isEmpty()) {
            HttpSession session = request.getSession();
            session.setAttribute("fieldError", "error");
            target = request.getContextPath() + "/comment/edit/" + id;
        } else {
            comment.setText(text);
            service.update(comment);
            target = request.getContextPath() + "/quote/" + comment.getQuote().getId();
        }
        response.sendRedirect(target);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = 0;
        try {
            String path = request.getPathInfo();
            id = Integer.parseInt(path.substring(1));
        } catch (NullPointerException | NumberFormatException ex) {
            response.sendRedirect(NOT_FOUND);
        }
        CommentService service = new CommentService();
        Comment comment = service.getById(id);
        request.setAttribute("comment", comment);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/editComment.jsp");
        rd.forward(request, response);
    }
}
