package ua.karghoff.quotes.web.servlet.user;

import ua.karghoff.quotes.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "Logout", urlPatterns = "/logout")
public class Logout extends HttpServlet {
    private static final String USER_SESSION_ATTRIBUTE = "user";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        User user = (User)session.getAttribute(USER_SESSION_ATTRIBUTE);

        if (user != null) {
            session.removeAttribute(USER_SESSION_ATTRIBUTE);
        }
        response.sendRedirect(request.getContextPath() + "/");
    }
}
