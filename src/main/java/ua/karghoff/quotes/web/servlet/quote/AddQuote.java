package ua.karghoff.quotes.web.servlet.quote;

import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.entity.Quote;
import ua.karghoff.quotes.service.impl.QuoteService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

@WebServlet(name = "AddQuote", urlPatterns = "/quote/add")
public class AddQuote extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QuoteService service = new QuoteService();
        HttpSession session = request.getSession();

        String text = request.getParameter("text");
        String author = request.getParameter("author");
        User user = (User) session.getAttribute("user");
        Timestamp ts = new Timestamp(new Date().getTime());

        String target;

        if ((text == null || text.trim().isEmpty()) || (author == null || author.trim().isEmpty())) {
            session.setAttribute("fieldError", "error");
            target = request.getContextPath() + "/quote/add";
        } else {
            Quote q = new Quote();
            q.setText(text);
            q.setUser(user);
            q.setAuthor(author);
            q.setDate(ts);
            q.setRating(0);

            service.insert(q);
            target = request.getContextPath() + "/";
        }
        response.sendRedirect(target);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/addQuote.jsp");
        rd.forward(request, response);
    }
}
