package ua.karghoff.quotes.web.servlet.quote;

import ua.karghoff.quotes.entity.Quote;
import ua.karghoff.quotes.service.impl.QuoteService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "ChangeRating", urlPatterns = "/quote/rating")
public class ChangeRating extends HttpServlet {
    private static final Integer ZERO = 0;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long id = Long.parseLong(request.getParameter("quote_id"));

        HttpSession session = request.getSession();
        Map<Long, Boolean> voted = (Map<Long, Boolean>) session.getAttribute("voted");

        Integer value = Integer.parseInt(request.getParameter("value"));
        value = value.compareTo(ZERO);

        QuoteService service = new QuoteService();
        Quote q = new Quote();
        q.setId(id);

        if (!voted.containsKey(id)) {
            service.changeRating(q, value);
            voted.put(id, true);
        }
        q = service.getById(id);

        String s = "{\"newRating\":" + q.getRating() + "}";

        response.getWriter().write(s);
    }
}
