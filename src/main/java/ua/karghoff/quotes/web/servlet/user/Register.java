package ua.karghoff.quotes.web.servlet.user;

import ua.karghoff.quotes.db.exception.DuplicateEntryException;
import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.util.Registration;
import ua.karghoff.quotes.service.exception.FieldsValidateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(displayName = "Register servlet", urlPatterns = "/register")
public class Register extends HttpServlet {
    private static final String registerPage = "/register.jsp";
    private static final String loginPage = "/login.jsp";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Registration registration = new Registration();

        String name = request.getParameter("name");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        String target = registerPage;
        try {
            User user = registration.register(name, password);
            target = loginPage;
            session.setAttribute("user", user);
        } catch (FieldsValidateException e) {
            session.setAttribute("fieldError", "error");
        } catch (DuplicateEntryException e) {
            session.setAttribute("duplicateError", "error");
        } finally {
            response.sendRedirect(request.getContextPath() + target);
        }
    }
}
