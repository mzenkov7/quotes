package ua.karghoff.quotes.web.servlet.comment;

import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.service.impl.CommentService;
import ua.karghoff.quotes.entity.Comment;
import ua.karghoff.quotes.entity.Quote;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

@WebServlet(name = "AddComment", urlPatterns = "/comment/add")
public class AddComment extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommentService service = new CommentService();
        HttpSession session = request.getSession();

        long quoteId = Long.parseLong(request.getParameter("quote-id"));
        String text = request.getParameter("text");
        if (text == null || text.trim().isEmpty()) {
            session.setAttribute("fieldError", "error");
        } else {
            User user = (User) session.getAttribute("user");
            Timestamp ts = new Timestamp(new Date().getTime());

            Quote q = new Quote();
            q.setId(quoteId);

            Comment comment = new Comment();
            comment.setUser(user);
            comment.setText(text);
            comment.setDate(ts);
            comment.setQuote(q);

            service.insert(comment);
        }
        response.sendRedirect(request.getContextPath() + "/quote/" + quoteId);
    }
}
