package ua.karghoff.quotes.web.servlet.quote;

import ua.karghoff.quotes.entity.Quote;
import ua.karghoff.quotes.service.impl.QuoteService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveQuote", urlPatterns = "/quote/remove")
public class RemoveQuote extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QuoteService service = new QuoteService();

        long id = Long.parseLong(request.getParameter("quote-id"));
        Quote quote = service.getById(id);
        service.delete(quote);

        response.sendRedirect(request.getContextPath() + "/");
    }
}
