package ua.karghoff.quotes.web.servlet.quote;

import ua.karghoff.quotes.entity.Quote;
import ua.karghoff.quotes.service.impl.QuoteService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "QuoteGet", urlPatterns = "/quote/*")
public class QuoteGet extends HttpServlet {
    private static final String NOT_FOUND = "/error/404.jsp";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = 0;
        try {
            String path = request.getPathInfo();
            id = Integer.parseInt(path.substring(1));
        } catch (NullPointerException | NumberFormatException ex) {
            response.sendRedirect(NOT_FOUND);
        }

        QuoteService service = new QuoteService();
        Quote quote = service.getById(id);
        request.setAttribute("quote", quote);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/quote.jsp");
        rd.forward(request, response);
    }
}
