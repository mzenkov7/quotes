package ua.karghoff.quotes.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

@WebServlet(name = "LocaleChanger", urlPatterns = "/localeChanger")
public class LocaleChanger extends HttpServlet {

    /**<p>Change supported languages</p>
     * get locale and set it in session scope
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        if (req.getParameter("locale") != null) {
            Locale locale = new Locale(req.getParameter("locale"));
            Config.set(session, Config.FMT_LOCALE, locale);
        }
        resp.sendRedirect(req.getHeader("referer"));
    }
}
