package ua.karghoff.quotes.web.servlet.quote;

import ua.karghoff.quotes.entity.Quote;
import ua.karghoff.quotes.service.impl.QuoteService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "EditQuote", urlPatterns = "/quote/edit/*")
public class EditQuote extends HttpServlet {
    private static final String NOT_FOUND = "/error/404.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QuoteService service = new QuoteService();

        String text = request.getParameter("text");
        String author = request.getParameter("author");
        long id = Long.parseLong(request.getParameter("quote-id"));

        String target;

        if (text == null || text.trim().isEmpty() || author == null || author.trim().isEmpty()) {
            HttpSession session = request.getSession();
            session.setAttribute("fieldError", "error");
            target = request.getContextPath() + "/quote/edit/" + id;
        } else {
            Quote quote = service.getById(id);
            quote.setText(text);
            quote.setAuthor(author);

            service.update(quote);
            target = request.getContextPath() + "/quote/" + id;
        }
        response.sendRedirect(target);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = 0;
        try {
            String path = request.getPathInfo();
            id = Integer.parseInt(path.substring(1));
        } catch (NullPointerException | NumberFormatException ex) {
            response.sendRedirect(NOT_FOUND);
        }

        QuoteService service = new QuoteService();
        Quote quote = service.getById(id);
        request.setAttribute("quote", quote);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/editQuote.jsp");
        rd.forward(request, response);
    }
}
