package ua.karghoff.quotes.web.servlet;

import ua.karghoff.quotes.service.impl.QuoteService;
import ua.karghoff.quotes.entity.Quote;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Random", urlPatterns = "/random")
public class Random extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QuoteService service = new QuoteService();
        List<Quote> list = service.getRandom(10);
        request.setAttribute("list", list);
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
        rd.forward(request, response);
    }
}
