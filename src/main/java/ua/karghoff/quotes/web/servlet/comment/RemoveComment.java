package ua.karghoff.quotes.web.servlet.comment;

import ua.karghoff.quotes.entity.Comment;
import ua.karghoff.quotes.service.impl.CommentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveComment", urlPatterns = "/comment/remove")
public class RemoveComment extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommentService service = new CommentService();

        long id = Long.parseLong(request.getParameter("comment-id"));
        Comment comment = service.getById(id);
        service.delete(comment);

        response.sendRedirect(request.getContextPath() + "/quote/" + comment.getQuote().getId());
    }
}
