package ua.karghoff.quotes.web.servlet;

import ua.karghoff.quotes.entity.Quote;
import ua.karghoff.quotes.service.impl.QuoteService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Best", urlPatterns = "/best")
public class Best extends HttpServlet {
    private static final int PER_PAGE = 10;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QuoteService service = new QuoteService();

        String pageParam = request.getParameter("page");
        int page = 0;
        if (pageParam != null) {
            try {
                page = Integer.parseInt(pageParam);
                if (page < 0) page = 0;
            } catch (NumberFormatException e) {
                // use default value 0
            }
        }

        List<Quote> list = service.getSorted("rating", true, page * PER_PAGE, PER_PAGE);
        long count = service.count();

        boolean hasMore = count > PER_PAGE * (page + 1);
        boolean hasBefore = page > 0;

        request.setAttribute("list", list);
        if (hasBefore) {
            request.setAttribute("prevPage", page - 1);
        }
        if (hasMore) {
            request.setAttribute("nextPage", page + 1);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
        rd.forward(request, response);
    }
}
