package ua.karghoff.quotes.entity;

public abstract class Entity {
    protected Long id;

    public Entity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
