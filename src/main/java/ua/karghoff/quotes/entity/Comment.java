package ua.karghoff.quotes.entity;

import java.sql.Timestamp;
import java.util.Objects;

public class Comment extends Entity {
    private String text;
    private User user;
    private Timestamp date;
    private Quote quote;

    public Comment() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Quote getQuote() {
        return quote;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(user, comment.user) &&
                Objects.equals(date, comment.date) &&
                Objects.equals(quote, comment.quote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, date, quote);
    }
}
