package ua.karghoff.quotes.entity;

public abstract class NamedEntity extends Entity {
    protected String name;

    public NamedEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
