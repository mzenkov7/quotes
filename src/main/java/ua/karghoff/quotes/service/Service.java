package ua.karghoff.quotes.service;

import ua.karghoff.quotes.entity.Entity;

import java.util.List;

public interface Service<T extends Entity> {

    T getById(long id);

    int insert(T entity);

    int delete(T entity);

    int update(T entity);

    List<T> getAll(int offset, int limit);

    long count();
}
