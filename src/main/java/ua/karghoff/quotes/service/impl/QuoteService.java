package ua.karghoff.quotes.service.impl;

import ua.karghoff.quotes.db.dao.SortedDAO;
import ua.karghoff.quotes.db.dao.impl.CommentDAO;
import ua.karghoff.quotes.db.dao.impl.QuoteDAO;
import ua.karghoff.quotes.entity.Comment;
import ua.karghoff.quotes.entity.Quote;

import java.util.List;

public class QuoteService extends GenericSortedService<Quote> {
    private QuoteDAO quoteDAO = new QuoteDAO();
    private CommentDAO commentDAO = new CommentDAO();

    @Override
    public Quote getById(long id) {
        Quote q = quoteDAO.getById(id);
        fillComments(q);
        return q;
    }

    @Override
    public List<Quote> getAll(int offset, int limit) {
        List<Quote> quotes = quoteDAO.getAll(offset, limit);
        fillComments(quotes);
        return quotes;
    }

    public List<Quote> getRandom(int limit) {
        List<Quote> quotes = getDao().getRandom(limit);
        fillComments(quotes);
        return quotes;
    }

    public List<Quote> getSorted(String columnName, boolean reverse, int offset, int limit) {
        List<Quote> quotes = getDao().getSorted(columnName, reverse, offset, limit);
        fillComments(quotes);
        return quotes;
    }

    public int changeRating(Quote quote, int diff) {
        if (diff == 0) {
            return 0;
        } else {
            return quoteDAO.changeRating(quote, diff);
        }
    }

    @Override
    public SortedDAO<Quote> getDao() {
        return quoteDAO;
    }

    private void fillComments(Quote quote) {
        List<Comment> comments = commentDAO.getByQuote(quote);
        quote.setComments(comments);
    }

    private void fillComments(List<Quote> quotes) {
        for (Quote q : quotes) {
            fillComments(q);
        }
    }
}
