package ua.karghoff.quotes.service.impl;

import ua.karghoff.quotes.db.dao.NamedDAO;
import ua.karghoff.quotes.service.NamedService;
import ua.karghoff.quotes.entity.NamedEntity;

public abstract class GenericNamedService<T extends NamedEntity> extends GenericService<T> implements NamedService<T> {

    public T getByName(String name) {
        return getDao().getByName(name);
    }

    @Override
    protected abstract NamedDAO<T> getDao();
}
