package ua.karghoff.quotes.service.impl;

import ua.karghoff.quotes.db.dao.SortedDAO;
import ua.karghoff.quotes.entity.Entity;
import ua.karghoff.quotes.service.SortedService;

import java.util.List;

public abstract class GenericSortedService<T extends Entity> extends GenericService<T> implements SortedService<T> {

    @Override
    public List<T> getRandom(int limit) {
        return getDao().getRandom(limit);
    }

    @Override
    public List<T> getSorted(String columnName, boolean reverse, int offset, int limit) {
        return getDao().getSorted(columnName, reverse, offset, limit);
    }

    @Override
    protected abstract SortedDAO<T> getDao();
}
