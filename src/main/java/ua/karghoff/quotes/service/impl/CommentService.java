package ua.karghoff.quotes.service.impl;

import ua.karghoff.quotes.db.dao.SortedDAO;
import ua.karghoff.quotes.db.dao.impl.CommentDAO;
import ua.karghoff.quotes.entity.Comment;

public class CommentService extends GenericSortedService<Comment> {
    private CommentDAO commentDAO = new CommentDAO();

    @Override
    public SortedDAO<Comment> getDao() {
        return commentDAO;
    }
}
