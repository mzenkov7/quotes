package ua.karghoff.quotes.service.impl;

import ua.karghoff.quotes.entity.Entity;
import ua.karghoff.quotes.service.Service;
import ua.karghoff.quotes.db.dao.DAO;

import java.util.List;

public abstract class GenericService<T extends Entity> implements Service<T> {

    public T getById(long id) {
        return getDao().getById(id);
    }

    public int insert(T entity) {
        return getDao().insert(entity);
    }

    public int delete(T entity) {
        return getDao().delete(entity);
    }

    public int update(T entity) {
        return getDao().update(entity);
    }

    public List<T> getAll(int offset, int limit) {
        return getDao().getAll(offset, limit);
    }

    public long count() {
        return getDao().count();
    }

    protected abstract DAO<T> getDao();
}
