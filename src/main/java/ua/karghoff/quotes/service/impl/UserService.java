package ua.karghoff.quotes.service.impl;

import ua.karghoff.quotes.entity.User;
import ua.karghoff.quotes.db.dao.NamedDAO;
import ua.karghoff.quotes.db.dao.impl.UserDAO;

import java.util.List;

public class UserService extends GenericNamedService<User> {

    UserDAO dao = new UserDAO();

    @Override
    public User getById(long id) {
        return dao.getById(id);
    }

    @Override
    public List<User> getAll(int offset, int limit) {
        return dao.getAll(offset, limit);
    }

    @Override
    protected NamedDAO<User> getDao() {
        return dao;
    }
}
