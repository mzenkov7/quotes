package ua.karghoff.quotes.service;

import ua.karghoff.quotes.entity.Entity;

import java.util.List;

public interface SortedService<T extends Entity> extends Service<T> {

    List<T> getRandom(int limit);

    List<T> getSorted(String columnName, boolean reverse, int offset, int limit);
}
