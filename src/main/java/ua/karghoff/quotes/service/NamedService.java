package ua.karghoff.quotes.service;

import ua.karghoff.quotes.entity.NamedEntity;

public interface NamedService<T extends NamedEntity> extends Service<T> {

    T getByName(String name);
}
