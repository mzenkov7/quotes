$('.rating-up').click(function () {
    var $idElem = $(this).siblings("input[type=hidden]");
    changeRating($idElem, +1);
});

$('.rating-down').click(function () {
    var $idElem = $(this).siblings("input[type=hidden]");
    changeRating($idElem, -1);
});

function changeRating($idElem, $value) {

    var $id = $idElem.val();
    var $ratingElem = $idElem.siblings(".rating");
    var $incElem = $idElem.siblings(".rating-up");
    var $decElem = $idElem.siblings(".rating-down");

    $.ajax({
        type: 'POST',
        url: ctx + '/quote/rating',
        dataType: 'json',
        data: {
            quote_id: $id,
            value: $value
        },

        success: function (data, textStatus, jqXHR) {
            $ratingElem.text(data.newRating);
            $incElem.remove();
            $decElem.remove();
        },

        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error occurred: \n' + errorThrown);
        }
    });
}
