<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html>
<head>
    <title>Edit comment</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <c:set var="comment" value="${requestScope.comment}"/>

    <form method="post" action="<c:url value="/comment/edit/${comment.id}"/>">
        <input type="hidden" name="comment-id" value="${comment.id}">
        <div><fmt:message key="comment.author"/> : ${comment.user.name}</div>
        <p>
            <label><fmt:message key="quote.text"/>
                <textarea rows="4" cols="100" name="text" maxlength="200">${comment.text}</textarea>
            </label>
        </p>
        <input type="submit" value="<fmt:message key="comment.edit"/>">
    </form>
    <form method="post" action="<c:url value="/comment/remove"/>">
        <input type="hidden" value="${comment.id}" name="comment-id">
        <input type="submit" value="<fmt:message key="comment.delete"/>">
    </form>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><fmt:message key="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>
</div>

<script src="<c:url value="/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/js/script.js"/>"></script>
</body>
</html>