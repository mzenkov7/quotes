<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html>
<head>
    <title>Quotes</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="quotes">
        <c:choose>
            <c:when test="${empty requestScope.list}">
                Nothing to show
            </c:when>
            <c:when test="${not empty requestScope.list}">
                <c:forEach var="quote" items="${requestScope.list}">
                    <div class="quote">
                        <div class="quote-info">
                            <div class="rating-info">
                                <input type="hidden" value="${quote.id}">
                                <c:if test="${empty sessionScope.voted[quote.id]}">
                                    <button class="rating-down"> -</button>
                                </c:if>
                                <span class="rating">${quote.rating}</span>
                                <c:if test="${empty sessionScope.voted[quote.id]}">
                                    <button class="rating-up"> +</button>
                                </c:if>
                                <span class="right"><fmt:formatDate value="${quote.date}"
                                                                    pattern="dd.MM.yyyy HH:mm:ss"/></span>
                            </div>
                        </div>
                        <hr>
                        <div class="quote-text">
                            <c:out value="${quote.text}"/>
                        </div>
                        <hr>
                        <div class="author">
                            <c:out value="${quote.author}"/>
                        </div>
                        <a class="comments-info" href="<c:url value="/quote/${quote.id}"/>" class="quote-id">
                            <fmt:message key="quotes.comments"/>: ${fn:length(quote.comments)}
                        </a>

                        <div style="clear: both"></div>
                    </div>
                </c:forEach>
            </c:when>
        </c:choose>
    </div>
    <c:if test="${not empty requestScope.prevPage}">
        <form class="left" method="get">
            <input type="hidden" value="${requestScope.prevPage}" name="page">
            <input type="submit" value="<fmt:message key="quotes.previous"/>">
        </form>
    </c:if>
    <c:if test="${not empty requestScope.nextPage}">
        <form class="right" method="get">
            <input type="hidden" value="${requestScope.nextPage}" name="page">
            <input type="submit" value="<fmt:message key="quotes.next"/>">
        </form>
    </c:if>
</div>
<script>var ctx = "${pageContext.request.contextPath}";</script>
<script src="<c:url value="/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/js/script.js"/>"></script>
</body>
</html>