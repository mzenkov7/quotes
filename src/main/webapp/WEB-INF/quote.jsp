<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html>
<head>
    <title>Add</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <c:set var="quote" value="${requestScope.quote}"/>
    <div class="quote">
        <div class="quote-info">
            <div class="rating-info">
                <input type="hidden" value="${quote.id}">
                <c:if test="${empty sessionScope.voted[quote.id]}">
                    <button class="rating-down"> - </button>
                </c:if>
                <span class="rating">${quote.rating}</span>
                <c:if test="${empty sessionScope.voted[quote.id]}">
                    <button class="rating-up"> + </button>
                </c:if>
                <span class="right"><fmt:formatDate value="${quote.date}" pattern="dd.MM.yyyy HH:mm:ss"/></span>
            </div>
        </div>
        <hr>
        <div class="quote-text">
            <c:out value="${quote.text}"/>
        </div>
        <hr>
        <div class="author">
            <c:out value="${quote.author}"/>
        </div>
        <c:if test="${not empty sessionScope.user and sessionScope.user.admin}">
            <form class="adm-btn" method="get" action="<c:url value="/quote/edit/${quote.id}"/>">
                <input type="submit" value="<fmt:message key="quote.edit"/>">
            </form>
            <form class="adm-btn" method="post" action="<c:url value="/quote/remove"/>">
                <input type="hidden" value="${quote.id}" name="quote-id">
                <input type="submit" value="<fmt:message key="quote.delete"/>">
            </form>
        </c:if>
        <div style="clear: both"></div>
    </div>

    <c:if test="${not empty sessionScope.user}">
        <form method="post" action="<c:url value="/comment/add"/>">
            <p><input type="hidden" name="quote-id" value="${quote.id}"></p>

            <p><textarea rows="4" cols="70" name="text" maxlength="200"></textarea></p>

            <p><input type="submit" value="<fmt:message key="comment.add"/>"></p>
        </form>
    </c:if>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><fmt:message key="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>

    <fmt:message key="quotes.comments"/> (${fn:length(quote.comments)})
    <hr>

    <c:if test="${not empty quote.comments}">
        <c:forEach var="comment" items="${quote.comments}">
            <div class="comment">
                <div class="comment-info">
                    <span>${comment.user.name}</span>
                    <span class="right"><fmt:formatDate value="${comment.date}" pattern="dd.MM.yyyy HH:mm:ss"/></span>
                </div>
                <hr>
                <div class="comment-text">
                    <c:out value="${comment.text}"/>
                </div>
                <c:if test="${not empty sessionScope.user and sessionScope.user.admin}">
                    <hr>
                    <form class="adm-btn" method="get" action="<c:url value="/comment/edit/${comment.id}"/>">
                        <input type="submit" value="<fmt:message key="comment.edit"/>">
                    </form>
                    <form class="adm-btn" method="post" action="<c:url value="/comment/remove"/>">
                        <input type="hidden" value="${comment.id}" name="comment-id">
                        <input type="submit" value="<fmt:message key="comment.delete"/>">
                    </form>
                </c:if>
            </div>
        </c:forEach>
    </c:if>
</div>
<script>var ctx = "${pageContext.request.contextPath}"</script>
<script src="<c:url value="/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/js/script.js"/>"></script>
</body>
</html>