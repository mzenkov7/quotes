<%@include file="/WEB-INF/include/page.jsp" %>

<div id="header">
    <div class="right">
        <%@include file="/WEB-INF/include/lang.jsp" %>
        <c:if test="${not empty sessionScope.user}">
            <form method="post" action="<c:url value="/logout"/>">
                <c:out value="${sessionScope.user.name} /"/>
                <input type="submit" value="<fmt:message key="logout.text"/>"/>
            </form>
        </c:if>
        <c:if test="${empty sessionScope.user}">
            <a href="<c:url value="/login.jsp"/>">Login</a> /
            <a href="<c:url value="/register.jsp"/>">Register</a>
        </c:if>
    </div>

    <ul id="nav">
        <li>
            <a href="<c:url value="/"/>"><fmt:message key="quotes.new"/></a>
        </li>
        <li>
            <a href="<c:url value="/random"/>"><fmt:message key="quotes.random"/></a>
        </li>
        <li>
            <a href="<c:url value="/best"/>"><fmt:message key="quotes.best"/></a>
        </li>
        <li>
            <a href="<c:url value="/quote/add"/>"><fmt:message key="quotes.add"/></a>
        </li>
    </ul>
    <div style="clear: both"></div>
</div>