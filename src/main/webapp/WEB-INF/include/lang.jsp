<%@include file="/WEB-INF/include/page.jsp" %>

<c:set var="localeCode" value="${pageContext.response.locale}" />

<form method="post" action="<c:url value="/localeChanger"/>">
    <select name="locale" onchange="this.form.submit()">
        <option value="ru" ${localeCode == 'ru' ? 'selected' : ''}><fmt:message key="lang.ru"/></option>
        <option value="en" ${localeCode == 'en' ? 'selected' : ''}><fmt:message key="lang.en"/></option>
    </select>
</form>