<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html>
<head>
    <title>Add quote</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <form method="post" action="<c:url value="/quote/add"/>">
        <p>
            <label><fmt:message key="quote.add.author"/>
                <input type="text" name="author" placeholder="<fmt:message key="quote.author"/>">
            </label>
        </p>
        <p>
            <label><fmt:message key="quote.add.text"/>
                <textarea rows="10" cols="45" name="text" maxlength="500" style="vertical-align: top"></textarea>
            </label>
        </p>

        <input type="submit" value="<fmt:message key="quotes.add.post"/>">
    </form>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><fmt:message key="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>
</div>

<script src="<c:url value="/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/js/script.js"/>"></script>
</body>
</html>