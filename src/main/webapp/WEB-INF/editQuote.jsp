<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html>
<head>
    <title>Edit quote</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <c:set var="quote" value="${requestScope.quote}"/>

    <form method="post" action="<c:url value="/quote/edit/${quote.id}"/>">
        <p><input type="hidden" name="quote-id" value="${quote.id}"></p>

        <p>
            <label><fmt:message key="quote.text"/>
                <textarea rows="10" cols="100" name="text" maxlength="500" style="vertical-align: top">
                    ${quote.text}
                </textarea>
            </label>
        </p>

        <p>
            <label><fmt:message key="quote.author"/>
                <input type="text" name="author" value="${quote.author}" placeholder="<fmt:message key="quote.author"/>">
            </label>
        </p>
        <input type="submit" value="<fmt:message key="quote.edit"/>">
    </form>
    <form method="post" action="<c:url value="/quote/remove"/>">
        <input type="hidden" value="${quote.id}" name="quote-id">
        <input type="submit" value="<fmt:message key="quote.delete"/>">
    </form>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><fmt:message key="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>
</div>

<script src="<c:url value="/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/js/script.js"/>"></script>
</body>
</html>