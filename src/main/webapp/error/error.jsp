<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html>
<head>
    <title>Oops, something bad happened</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="centered">
        <h2 class="errorPageInfo">Oops, something bad happened!</h2>
        <p class="errorPageInfo"><%= exception %></p>
        <img src="<c:url value="/img/error.jpg"/>" alt="Error"/>
    </div>
</div>
</body>
</html>