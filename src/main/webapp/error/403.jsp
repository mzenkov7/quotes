<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Forbidden</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>
    <div class="centered">
        <h2 class="errorPageInfo">Forbidden content</h2>
        <img src="<c:url value="/img/403.png"/>" alt="Access denied"/>
    </div>
</div>
</body>
</html>
