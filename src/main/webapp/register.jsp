<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>Auth</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>
<body style="background-color: #ddd;">

<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="centered">
        <c:if test="${not empty sessionScope.fieldError}">
            <p class="error-message"><fmt:message key="error.fieldError"/></p>
            <c:remove var="fieldError" scope="session"/>
        </c:if>

        <c:if test="${not empty sessionScope.duplicateError}">
            <p class="error-message"><fmt:message key="error.duplicateError"/></p>
            <c:remove var="duplicateError" scope="session"/>
        </c:if>

        <form method="post" action="<c:url value="/register"/>">
            <p>
                <input type="text" name="name" value="" style="width: 100%"
                       placeholder="<fmt:message key="form.name"/>"/>
            </p>
            <p>
                <input type="password" name="password" value="" style="width: 100%"
                       placeholder="<fmt:message key="form.password"/>"/>
            </p>
            <input type="submit" value="<fmt:message key="register.form.button"/>"/>
        </form>
    </div>
</div>

</body>
</html>